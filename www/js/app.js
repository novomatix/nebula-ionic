// Ionic Starter App

angular.module('underscore', [])
.factory('_', function() {
  return window._; // assumes underscore has already been loaded on the page
});

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('nEBULA', [
  'ionic',
  'nEBULA.directives',
  'nEBULA.controllers',
  'nEBULA.views',
  'nEBULA.services',
  'nEBULA.config',
  'nEBULA.factories',
  'nEBULA.filters',
  'ngMap',
  'angularMoment',
  'underscore',
  'ngCordova',
  'ngCookies',
  'youtube-embed',
  'pascalprecht.translate'  // inject the angular-translate module
  ])

.run(function($ionicPlatform, AuthService, $rootScope, $state, $ionicHistory, PushNotificationsService) {

  $ionicPlatform.on("deviceready", function(){

    AuthService.userIsLoggedIn().then(function(response)
    {
      if(response === true)
      {
        //update user avatar and go on
        AuthService.updateUserAvatar();
      }
      $state.go('app.pages');
      // else
      // {
      //   $state.go('walkthrough');
      // }
    });

  $ionicPlatform.registerBackButtonAction(function (event) {
    if ($ionicHistory.currentStateName() === 'app.lesson' || $ionicHistory.currentStateName() === 'app.quiz'){
      event.preventDefault();
    } else {
      $ionicHistory.goBack();
    }
  }, 100);

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
  if (window.cordova && window.cordova.plugins.Keyboard) {
    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
  }
  if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }

    PushNotificationsService.register();

  });

  $ionicPlatform.on("resume", function(){
    AuthService.userIsLoggedIn().then(function(response)
    {
      if(response === false)
      {
        // $state.go('walkthrough');
        $state.go('app.pages');
      } else{
        //update user avatar and go on
        AuthService.updateUserAvatar();
      }
    });

    PushNotificationsService.register();
  });

  // UI Router Authentication Check
  $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
    if (toState.data.authenticate)
    {
      AuthService.userIsLoggedIn().then(function(response)
      {
        if(response === false)
        {
          event.preventDefault();
          $ionicHistory.nextViewOptions({disableBack: true});
          $state.go('app.walkthrough');
        }
      });
    }
  });
})

.config(function($ionicConfigProvider, $stateProvider, $urlRouterProvider, $translateProvider) {
  
    $translateProvider
      .useStaticFilesLoader({
        prefix: 'lang/',
        suffix: '.json'
      })
      .registerAvailableLanguageKeys(['en', 'el', 'nl', 'es', 'tr'], {
        'en' : 'en',
        'el' : 'el',
        'nl' : 'nl',
        'es' : 'es',
        'tr' : 'tr'
      })
      .preferredLanguage('en')
      .fallbackLanguage('en')
      .determinePreferredLanguage()
      .useLocalStorage()
      .useSanitizeValueStrategy('escapeParameters');

  $stateProvider

  .state('app.walkthrough', {
    url: "/",
    views: {
      'menuContent': {
        templateUrl: "views/auth/walkthrough.html",
        controller: 'WalkthroughCtrl'
      }
    },
    data: {
      authenticate: false
    }
  })

  .state('app.register', {
    url: "/register",
    views: {
      'menuContent': {
        templateUrl: "views/auth/register.html",
        controller: 'RegisterCtrl'
      }
    },
    data: {
      authenticate: false
    }
  })

  .state('app.login', {
    url: "/login",
    views: {
      'menuContent': {
        templateUrl: "views/auth/login.html",
        controller: 'LoginCtrl'
      }
    },
    data: {
      authenticate: false
    }
  })

  .state('app.forgot_password', {
    url: "/forgot_password",
    views: {
      'menuContent': {
        templateUrl: "views/auth/forgot-password.html",
        controller: 'ForgotPasswordCtrl'
      }
    },
    data: {
      authenticate: false
    }
  })

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "views/app/side-menu.html",
    controller: 'AppCtrl'
  })


  .state('app.pages', {
    url: "/pages/:slug",
    views: {
      'menuContent': {
        templateUrl: "views/app/wordpress/pages.html",
        controller: 'PagesCtrl'
      }
    },
    data: {
      authenticate: false
    }
  })

  .state('app.page', {
    url: "/wordpress_page/:pageId",
    views: {
      'menuContent': {
        templateUrl: "views/app/wordpress/page.html",
        controller: 'PageCtrl'
      }
    },
    data: {
      authenticate: false
    },
    resolve: {
      page_data: function(PostService, $ionicLoading, $filter, $stateParams) {
        $ionicLoading.show({
          template: $filter('translate')('TXT_LOADING')
        });
        var pageId = $stateParams.pageId;
        return PostService.getWordpressPage(pageId);
      }
    }
  })

  .state('app.pageSlug', {
    url: "/wordpress_pageSlug/:slug",
    views: {
      'menuContent': {
        templateUrl: "views/app/wordpress/page.html",
        controller: 'PageCtrl'
      }
    },
    data: {
      authenticate: false
    },
    resolve: {
      page_data: function(PostService, $ionicLoading, $filter, $stateParams) {
        $ionicLoading.show({
          template: $filter('translate')('TXT_LOADING')
        });
        var slug = $stateParams.slug;
        return PostService.getWordpressPageSlug(slug);
      }
    }
  })

  .state('app.courses', {
    url: "/courses",
    views: {
      'menuContent': {
        templateUrl: "views/app/wordpress/courses.html",
        controller: 'CoursesCtrl'
      }
    },
    data: {
      authenticate: false
    }
  })

  .state('app.course', {
    url: "/course/:courseId",
    views: {
      'menuContent': {
        templateUrl: "views/app/wordpress/course.html",
        controller: 'CourseCtrl'
      }
    },
    data: {
      authenticate: false
    },
    resolve: {
      course_data: function(PostService, $ionicLoading, $filter, $stateParams) {
        $ionicLoading.show({
          template: $filter('translate')('TXT_LOADING')
        });
        var courseId = $stateParams.courseId;
        return PostService.getPost(courseId, 'course');
      }
    }
  })

  .state('app.lesson', {
    url: "/course/lesson/:lessonId",
    views: {
      'menuContent': {
        templateUrl: "views/app/wordpress/lesson.html",
        controller: 'LessonCtrl'
      }
    },
    data: {
      authenticate: true
    },
    resolve: {
      lesson_data: function(PostService, $ionicLoading, $filter, $stateParams) {
        $ionicLoading.show({
          template: $filter('translate')('TXT_LOADING')
        });
        var lessonId = $stateParams.lessonId;
        return PostService.getPost(lessonId, 'lesson');
      }
    }
  })

  .state('app.quiz', {
    url: "/course/lesson/quiz/:quizId",
    views: {
      'menuContent': {
        templateUrl: "views/app/wordpress/quiz.html",
        controller: 'QuizCtrl'
      }
    },
    data: {
      authenticate: true
    },
    resolve: {
      quiz_data: function(PostService, $ionicLoading, $filter, $stateParams) {
        $ionicLoading.show({
          template: $filter('translate')('TXT_LOADING')
        });
        var quizId = $stateParams.quizId;
        return PostService.getPost(quizId, 'quiz');
      }
    }
  })

  .state('app.post', {
    url: "/post/:postId",
    views: {
      'menuContent': {
        templateUrl: "views/app/wordpress/post.html",
        controller: 'PostCtrl'
      }
    },
    data: {
      authenticate: false
    },
    resolve: {
      post_data: function(PostService, $ionicLoading, $filter, $stateParams) {
        $ionicLoading.show({
          template: $filter('translate')('TXT_LOADING')
        });

        var postId = $stateParams.postId;
        return PostService.getPost(postId, '');
      }
    }
  })

  .state('app.category', {
    url: "/category/:categoryTitle/:categoryId",
    views: {
      'menuContent': {
        templateUrl: "views/app/wordpress/category.html",
        controller: 'PostCategoryCtrl'
      }
    },
    data: {
      authenticate: false
    }
  })

  .state('app.contact', {
    url: "/contact",
    views: {
      'menuContent': {
        templateUrl: "views/app/contact.html",
        controller: 'ContactCtrl'
      }
    },
    data: {
      authenticate: false
    }
  })

  .state('app.bookmarks', {
    url: "/bookmarks",
    views: {
      'menuContent': {
        templateUrl: "views/app/bookmarks.html",
        controller: 'BookMarksCtrl'
      }
    },
    data: {
      authenticate: true
    }
  })

  .state('app.settings', {
    url: "/settings",
    views: {
      'menuContent': {
        templateUrl: "views/app/settings.html",
        controller: 'SettingCtrl'
      }
    },
    data: {
      authenticate: false
    }
  })

  .state('app.langselect', {
    url: "/langselect",
    views: {
      'menuContent': {
        templateUrl: "views/app/langselect.html",
        controller: 'LangSelectCtrl'
      }
    },
    data: {
      authenticate: false
    }
  })

  .state('app.profile', {
    url: "/profile",
    views: {
      'menuContent': {
        templateUrl: "views/app/profile.html",
        controller: 'ProfileCtrl'
      }
    },
    data: {
      authenticate: true
    }
  })  


  ;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/pages/');
})

;
