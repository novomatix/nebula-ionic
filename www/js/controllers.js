angular.module('nEBULA.controllers', [])

// APP - RIGHT MENU
.controller('AppCtrl', function($scope, $translate, $timeout, AuthService) {
  $scope.$on('$ionicView.enter', function(){
    // Refresh user data & avatar
    var currentLang = $translate.storage().get(  $translate.storageKey() ) ||  $translate.proposedLanguage();
    $translate.use(currentLang);
    $scope.user = AuthService.getUser();
  });
})

// CATEGORIES MENU
.controller('PushMenuCtrl', function($scope, Categories) {

  var getItems = function(parents, categories){

    if(parents.length > 0){

      _.each(parents, function(parent){
        parent.name = parent.title;
        parent.link = parent.slug;

        var items = _.filter(categories, function(category){ return category.parent===parent.id; });

        if(items.length > 0){
          parent.menu = {
            title: parent.title,
            id: parent.id,
            items:items
          };
          getItems(parent.menu.items, categories);
        }
      });
    }
    return parents;
  };

  Categories.getCategories()
  .then(function(data){
    var sorted_categories = _.sortBy(data.categories, function(category){ return category.title; });
    var parents = _.filter(sorted_categories, function(category){ return category.parent===0; });
    var result = getItems(parents, sorted_categories);

    $scope.menu = {
      title: 'All Categories',
      id: '0',
      items: result
    };
  });
})


// BOOKMARKS
.controller('BookMarksCtrl', function($scope, $rootScope, BookMarkService) {

  $scope.bookmarks = BookMarkService.getBookmarks();

  // When a new post is bookmarked, we should update bookmarks list
  $rootScope.$on("new-bookmark", function(event, post_id){
    $scope.bookmarks = BookMarkService.getBookmarks();
  });

  $scope.remove = function(bookmarkId) {
    BookMarkService.remove(bookmarkId);
    $scope.bookmarks = BookMarkService.getBookmarks();
  };
})


// CONTACT
.controller('ContactCtrl', function($scope) {

  //map
  $scope.position = {
    lat: 43.07493,
    lng: -89.381388
  };

  $scope.$on('mapInitialized', function(event, map) {
    $scope.map = map;
  });
})

// LANGUAGE SELECTION
.controller('LangSelectCtrl', function($scope, $ionicActionSheet, $ionicModal, $ionicHistory, $state, $translate, $timeout, $ionicPopup, $filter, $window, AuthService) {
  $scope.switchLanguage = function(key) {
    var confirmPopup = $ionicPopup.confirm({
     title: $filter('translate')('TXT_CHANGE_LANGUAGE'),
     template: $filter('translate')('TXT_ARE_YOU_SURE_YOU_WANT_TO_CHANGE_LANGUAGE'),
   });

    confirmPopup.then(function(res) {
     if(res) {
       $ionicHistory.clearCache();
       $ionicHistory.clearHistory();
       $translate.use(key);
     } else {
       console.log('lang no change');
     }
   });
  };
})

// SETTINGS
.controller('SettingCtrl', function($scope, $ionicActionSheet, $ionicModal, $state, AuthService) {
  $scope.notifications = true;
  $scope.sendLocation = true;

  $ionicModal.fromTemplateUrl('views/common/terms.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.terms_modal = modal;
  });

  $ionicModal.fromTemplateUrl('views/common/faqs.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.faqs_modal = modal;
  });

  $ionicModal.fromTemplateUrl('views/common/credits.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.credits_modal = modal;
  });

  $scope.showTerms = function() {
    $scope.terms_modal.show();
  };

  $scope.showFAQS = function() {
    $scope.faqs_modal.show();
  };

  $scope.showCredits = function() {
    $scope.credits_modal.show();
  };

})




// PROFILE
.controller('ProfileCtrl', function($scope, $ionicActionSheet, $ionicModal, $ionicHistory, $state, $ionicLoading, $timeout, $filter, AuthService) {

 $scope.user = AuthService.getUser().userdata;

 $scope.updateUser = function(){

  $ionicLoading.show({
    template: $filter('translate')('TXT_UPDATING_USER_DATA')
  });

  var userdata = {
    firstname: $scope.user.firstname,
    lastname: $scope.user.lastname,
    description: $scope.user.description
  };

  AuthService.updateUser(userdata)
  .then(function(data){
        //success
        $scope.message = 'Profile updated successfully';
        $timeout(function(){$scope.message = false;}, 5000);
        $ionicLoading.hide();
      },function(err){
        //err
        $scope.error = err;
        $timeout(function(){$scope.error = false;}, 5000);
        $ionicLoading.hide();
      });
};
   // Triggered on a the logOut button click
   $scope.showLogOutMenu = function() {

    // Show the action sheet
    var hideSheet = $ionicActionSheet.show({
      //Here you can add some more buttons
      // buttons: [
      // { text: '<b>Share</b> This' },
      // { text: 'Move' }
      // ],
      destructiveText: $filter('translate')('TXT_LOGOUT'),
      titleText: $filter('translate')('TXT_ARE_YOU_SURE_YOU_WANT_TO_LOGOUT'),
      cancelText: $filter('translate')('TXT_CANCEL'),
      cancel: function() {
        // add cancel code..
      },
      buttonClicked: function(index) {
        //Called when one of the non-destructive buttons is clicked,
        //with the index of the button that was clicked and the button object.
        //Return true to close the action sheet, or false to keep it opened.
        return true;
      },
      destructiveButtonClicked: function(){
        //Called when the destructive button is clicked.
        //Return true to close the action sheet, or false to keep it opened.
        AuthService.logOut();
        $ionicHistory.nextViewOptions({disableBack: true});
        $state.go('app.pages');
      }
    });
  };
})

//EMAIL SENDER
.controller('EmailSenderCtrl', function($scope, $cordovaEmailComposer) {

  $scope.sendFeedback = function(){
    cordova.plugins.email.isAvailable(
      function (isAvailable) {
        // alert('Service is not available') unless isAvailable;
        cordova.plugins.email.open({
          to:      'john@doe.com',
          cc:      'jane@doe.com',
          subject: 'Feedback',
          body:    'This app is awesome'
        });
      }
      );
  };

  $scope.sendContactMail = function(){
    //Plugin documentation here: http://ngcordova.com/docs/plugins/emailComposer/

    $cordovaEmailComposer.isAvailable().then(function() {
      // is available
      $cordovaEmailComposer.open({
        to: 'john@doe.com',
        cc: 'sally@doe.com',
        subject: 'Contact from ionWordpress',
        body: 'How are you? Nice greetings from Uruguay'
      })
      .then(null, function () {
          // user cancelled email
        });
    }, function () {
      // not available
    });
  };

})


// RATE THIS APP
.controller('RateAppCtrl', function($scope) {

  $scope.rateApp = function(){
    if(ionic.Platform.isIOS()){
      AppRate.preferences.storeAppURL.ios = '<my_app_id>';
      AppRate.promptForRating(true);
    }else if(ionic.Platform.isAndroid()){
      AppRate.preferences.storeAppURL.android = 'market://details?id=<package_name>';
      AppRate.promptForRating(true);
    }
  };
})


// WALKTHROUGH
.controller('WalkthroughCtrl', function($scope, $state, $ionicLoading, $ionicSlideBoxDelegate) {

  $scope.$on('$ionicView.enter', function(){
    //this is to fix ng-repeat slider width:0px;
    $ionicSlideBoxDelegate.$getByHandle('walkthrough-slider').update();
    $ionicLoading.hide();
  });
})

//LOGIN
.controller('LoginCtrl', function($scope, $state, $ionicLoading, $ionicHistory, $filter, AuthService, PushNotificationsService) {
  $scope.user = {};

  $scope.doLogin = function(){

    $ionicLoading.show({
      template: $filter('translate')('TXT_LOGING_IN')
    });

    var user = {
      userName: $scope.user.userName,
      password: $scope.user.password
    };

    AuthService.doLogin(user)
    .then(function(user){
      //success
      $ionicHistory.nextViewOptions({disableBack: true});
      $state.go('app.profile');
      $ionicLoading.hide();
    },function(err){
      //err
      $scope.error = err;
      $ionicLoading.hide();
    });
  };
})


// FORGOT PASSWORD
.controller('ForgotPasswordCtrl', function($scope, $state, $ionicLoading, $timeout, $filter, AuthService) {
  $scope.user = {};

  $scope.recoverPassword = function(){

    $ionicLoading.show({
      template: $filter('translate')('TXT_RECOVERING_PASSWORD')
    });

    AuthService.doForgotPassword($scope.user.userName)
    .then(function(data){
      if(data.status == "error"){
        $scope.error = data.error;
        $timeout(function(){$scope.error = false;}, 3000);  
      }else{
        $scope.message ="Link for password reset has been emailed to you. Please check your email.";
        $timeout(function(){$scope.message = false;}, 3000);  
      }
      $ionicLoading.hide();
    });
  };
})


// REGISTER
.controller('RegisterCtrl', function($scope, $state, $ionicLoading, $timeout, $filter, AuthService, PushNotificationsService, PostService) {
  $scope.user = {};
  PostService.getCategories('course_category')
  .then(function(data){
    $scope.categories = data.categories;
  });
  
  $scope.doRegister = function(){

    $ionicLoading.show({
      template: $filter('translate')('TXT_REGISTERING_USER')
    });

    var user = {
      userName: $scope.user.userName,
      email: $scope.user.email,
      displayName: $scope.user.displayName,
      password: $scope.user.password,
      institution: $scope.user.institution
    };

    AuthService.doRegister(user)
    .then(function(data){
      //success
      $scope.message = data;
      $timeout(function(){$scope.message = false;}, 3000);
      $ionicLoading.hide();
    },function(err){
      //err
      $scope.error = err;
      $timeout(function(){$scope.error = false;}, 3000);
      $ionicLoading.hide();
    });
  };
})

// HOME - GET RECENT POSTS
.controller('HomeCtrl', function($scope, $rootScope, $state, $ionicLoading, $filter, PostService) {
  $scope.posts = [];
  $scope.page = 1;
  $scope.totalPages = 1;

  $scope.doRefresh = function() {
    $ionicLoading.show({
      template: $filter('translate')('TXT_LOADING')
    });

    //Always bring me the latest posts => page=1
    PostService.getRecentPosts(1)
    .then(function(data){

      $scope.totalPages = data.pages;
      $scope.posts = PostService.shortenPosts(data.posts);
      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
    });
  };

  $scope.loadMoreData = function(){
    $scope.page += 1;

    PostService.getRecentPosts($scope.page)
    .then(function(data){
      //We will update this value in every request because new posts can be created
      $scope.totalPages = data.pages;
      var new_posts = PostService.shortenPosts(data.posts);
      $scope.posts = $scope.posts.concat(new_posts);

      $scope.$broadcast('scroll.infiniteScrollComplete');
    });
  };

  $scope.moreDataCanBeLoaded = function(){
    return $scope.totalPages > $scope.page;
  };

  $scope.sharePost = function(link){
    PostService.sharePost(link);
  };

  $scope.bookmarkPost = function(post){
    $ionicLoading.show({ template: 'Post Saved!', noBackdrop: true, duration: 1000 });
    PostService.bookmarkPost(post);
  };

  $scope.doRefresh();

})

// PAGES
.controller('PagesCtrl', function($scope, $rootScope, $state, $ionicLoading, $filter, $translate, $window, PostService) {
  $scope.posts = [];
  $scope.page = 1;
  $scope.totalPages = 1;
  if(!$state.params.slug) $state.params.slug = 'LINK_ABOUT';

  $scope.doRefresh = function(){
    $ionicLoading.show({
      template: $translate('TXT_LOADING')
    });

    PostService.getWordpressPageSlug($state.params.slug)
    .then(function(data){
      if(data.status=='error') $window.location.reload(true);
      var tmpPageId = data.page.id;
      PostService.getPages(tmpPageId)
      .then(function(data){
        $scope.totalPages = data.pages;
        $scope.pages = data.pages;
        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
      });
    });
  };

  $scope.loadMoreData = function(){
    $scope.page += 1;

    PostService.getRecentPosts($scope.page)
    .then(function(data){
      //We will update this value in every request because new posts can be created
      $scope.totalPages = data.pages;
      var new_posts = PostService.shortenPosts(data.posts);
      $scope.posts = $scope.posts.concat(new_posts);

      $scope.$broadcast('scroll.infiniteScrollComplete');
    });
  };

  $scope.moreDataCanBeLoaded = function(){
    return $scope.totalPages > $scope.page;
  };

  $scope.sharePost = function(link){
    PostService.sharePost(link);
  };

  $scope.bookmarkPost = function(post){
    $ionicLoading.show({ template: 'Post Saved!', noBackdrop: true, duration: 1000 });
    PostService.bookmarkPost(post);
  };

  $scope.doRefresh();

})

// WP PAGE
.controller('PageCtrl', function($scope, page_data, $ionicLoading) {
  $scope.page = page_data.page;
  $ionicLoading.hide();
})



// COURSES
.controller('CoursesCtrl', function($scope, $rootScope, $state, $ionicLoading, $filter, PostService) {
  $scope.courses = [];
  $scope.page = 1;
  $scope.totalPages = 1;

  $scope.doRefresh = function() {
    $ionicLoading.show({
      template: $filter('translate')('TXT_LOADING')
    });

    //Always bring me the latest posts => page=1
    PostService.getRecentCourses(1)
    .then(function(data){
      $scope.totalPages = data.pages;
      $scope.courses = PostService.shortenPosts(data.posts);
      angular.forEach($scope.courses, function(course){
        PostService.getMeta(course.id, '_course_status')
        .then(function(data){
          course._status = data;
        });
      });
      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
    });
  };

  $scope.loadMoreData = function(){
    $scope.page += 1;

    PostService.getRecentCourses($scope.page)
    .then(function(data){
      //We will update this value in every request because new courses can be created
      $scope.totalPages = data.pages;
      var new_posts = PostService.shortenPosts(data.posts);
      $scope.courses = $scope.courses.concat(new_posts);

      $scope.$broadcast('scroll.infiniteScrollComplete');
    });
  };

  $scope.moreDataCanBeLoaded = function(){
    return $scope.totalPages > $scope.page;
  };

  $scope.doRefresh();

})


// COURSE
.controller('CourseCtrl', function($scope, course_data, $ionicLoading, $timeout, $ionicHistory, PostService, AuthService, $filter, $ionicScrollDelegate) {
  $ionicHistory.nextViewOptions({
   disableBack: true
 });
  $scope.course = course_data.post;
  //Bring me the course lessons
  var user = AuthService.getUser();
  $scope.isLoggedin = (user.userdata !== null) ? true : false;
  PostService.getCourseData(course_data.post.id)
  .then(function(data){
    angular.forEach(data.lessons, function(lesson){
      PostService.getLessonData(lesson.ID)
      .then(function(data){
        lesson.status = data.status;
        lesson.progress = data.progress;
        lesson.hasQuiz = _.isEmpty(data.quiz) ?  false : true;
        //lesson.attachments  = (user.userdata !== null) ? data.attachments : null;
      });
    });
    $scope.lessons = data.lessons;
    $scope.isMember = data.isMember;
    $scope.canRetake = data.canRetake;
    $scope.progress = data.progress;
    $scope.isSameInst = data.author_inst == data.user_inst ? true : false;
    $scope.isPrivate = data.status == 'private' ? true : false;
    $scope.displayAction = true;
    $ionicLoading.hide();
  });

  $scope.courseUnsubsribe = function(){
    $ionicLoading.show({
      template: $filter('translate')('TXT_UNSUBSCRIBING_FROM_COURSE')
    });

    PostService.userCourseUnsubsribe(course_data.post.id)
    .then(function(data){
      if(data.status == "error"){
        $scope.error = data.error;
        $timeout(function(){$scope.error = false;}, 3000);  
      }else{
        $scope.isMember = false;
        $scope.message = data.message;
        $timeout(function(){$scope.message = false;}, 5000);
      }
      $ionicLoading.hide();
    },function(err){
        //err
        $scope.error = err;
        $timeout(function(){$scope.error = false;}, 5000);
        $ionicLoading.hide();
      });
  };

  $scope.courseSubsribe = function(){
    var template = ($scope.isPrivate && !$scope.isSameInst) ? $filter('translate')('TXT_SENDING_REQUEST_TO_AUTHOR') : $filter('translate')('TXT_SUBSCRIBING_TO_COURSE');
    $ionicLoading.show({
      template: template
    });

    PostService.userCourseSubsribe(course_data.post.id)
    .then(function(data){
      if(data.status == "error"){
        $scope.error = data.error;
        $timeout(function(){$scope.error = false;}, 8000);  
      }else{
        if(data.action == 'subscribe') {
          $scope.isMember = true;
        }
        $scope.message = data.message;
        $timeout(function(){$scope.message = false;}, 8000);
      }
      $ionicLoading.hide();
    },function(err){
        //err
        $scope.error = err;
        $timeout(function(){$scope.error = false;}, 8000);
        $ionicLoading.hide();
      });
  };


})

.controller('LessonCtrl', function($scope, lesson_data, $ionicLoading, $ionicHistory, $timeout, PostService, AuthService, $filter, $ionicScrollDelegate) {
  $ionicHistory.nextViewOptions({
   disableBack: true
 });
  $scope.lesson = lesson_data.post;
  PostService.getLessonData(lesson_data.post.id)
  .then(function(data){

   $scope.isMember = data.isMember;
   $scope.course = data.course;
   data.progress = (data.progress !== false) ?  data.progress : 0;
   $scope.progress = data.progress;
   data.prerequisite.progress = (data.prerequisite.progress !== false) ?  data.prerequisite.progress : 0;
   $scope.prerequisite = data.prerequisite;
   $scope.canRetake = data.canRetake;
   $scope.hide = data.hide;
   $scope.quiz = data.quiz;
   $scope.hasQuiz = _.isEmpty(data.quiz) ?  false : true;
   $scope.course = data.course;
   $scope.prev = data.prev;
   $scope.next = data.next;
   $scope.lesson.status = data.status;
   $scope.lesson.attachments = data.attachments;
   $scope.showLesson = true;
 });
  $ionicLoading.hide();

  $scope.lessonComplete = function(){
    $ionicLoading.show({
      template: $filter('translate')('TXT_COMPLETING_LESSON')
    });

    PostService.userLessonComplete(lesson_data.post.id)
    .then(function(data){
      if(data.status == "error"){
        $scope.error = data.error;
        $timeout(function(){$scope.error = false;}, 8000);  
      }else{
        $scope.message = data.message;
        $scope.progress = data.progress;
        $timeout(function(){$scope.message = false;}, 8000);
      }
      $ionicLoading.hide();
    },function(err){
        //err
        $scope.error = err;
        $timeout(function(){$scope.error = false;}, 8000);
        $ionicLoading.hide();
      });
  };

  $scope.lessonIncomplete = function(){
    $ionicLoading.show({
      template: $filter('translate')('TXT_INCOMPLETING_LESSON')
    });

    PostService.userLessonInComplete(lesson_data.post.id)
    .then(function(data){
      if(data.status == "error"){
        $scope.error = data.error;
        $timeout(function(){$scope.error = false;}, 8000);  
      }else{
        $scope.message = data.message;
        $scope.progress = 0;
        $timeout(function(){$scope.message = false;}, 8000);
      }
      $ionicLoading.hide();
    },function(err){
        //err
        $scope.error = err;
        $timeout(function(){$scope.error = false;}, 8000);
        $ionicLoading.hide();
      });
  };

})

.controller('QuizCtrl', function($scope, quiz_data, $ionicLoading, $timeout, $ionicHistory, PostService, AuthService, $filter, $ionicScrollDelegate) {
  $ionicHistory.nextViewOptions({
   disableBack: true
 });

  $scope.quiz = quiz_data.post;

  PostService.getQuizData(quiz_data.post.id)
  .then(function(data){
    $scope.course     = data.course;
    $scope.lesson     = data.lesson;
    data.progress     = (data.progress !== false) ?  data.progress : 0;
    $scope.progress   = data.progress;
    data.prerequisite = (data.prerequisite !== false) ?  data.prerequisite : 100;
    $scope.prerequisite = data.prerequisite;
    $scope.isMember   = data.isMember;
    var filter = Object.keys(data.questions), q = [];
    angular.forEach(filter, function(key, value){
      var tmp = {'key': key};
      q.push(_.extend(tmp, data.questions[key]));
    });
    $scope.questions = q;
    $ionicLoading.hide();
  });


  $scope.quizComplete = function(data){
   $ionicLoading.show({
    template: $filter('translate')('TXT_COMPLETING_QUIZ')
  });
   PostService.userQuizComplete(quiz_data.post.id, data)
   .then(function(data){
    if(data.status == "error"){
      $scope.error = data.message;
      PostService.getQuizData(quiz_data.post.id)
      .then(function(data){
        var filter = Object.keys(data.questions), q = [];
        angular.forEach(filter, function(key, value){
          var tmp = {'key': key};
          q.push(_.extend(tmp, data.questions[key]));
        });
        $scope.questions = q;
        $ionicLoading.hide();
      });
      $timeout(function(){$scope.error = false;}, 3000);  
    }else{
      $scope.message = data.message;
      $scope.progress = data.progress;
      $timeout(function(){$scope.message = false;}, 5000);
      $ionicLoading.hide();
    }
  },function(err){
        //err
        $scope.error = err;
        $timeout(function(){$scope.error = false;}, 5000);
        $ionicLoading.hide();
      });
 };


})

// POST
.controller('PostCtrl', function($scope, post_data, $ionicLoading, PostService, AuthService, $filter, $ionicScrollDelegate) {
  $scope.post = post_data.post;
  $scope.comments = _.map(post_data.post.comments, function(comment){
    if(comment.author){
      PostService.getUserGravatar(comment.author.id)
      .then(function(avatar){
        comment.user_gravatar = avatar;
      });
      return comment;
    }else{
      return comment;
    }
  });
  $ionicLoading.hide();

  $scope.addComment = function(){

    $ionicLoading.show({
      template: $filter('translate')('TXT_SUBMITING_COMMENT')
    });

    PostService.submitComment($scope.post.id, $scope.new_comment)
    .then(function(data){
      if(data.status=="ok"){
        var user = AuthService.getUser();

        var comment = {
          author: {name: user.userdata.username},
          content:$scope.new_comment,
          date: Date.now(),
          user_gravatar : user.avatar,
          id: data.comment_id
        };
        $scope.comments.push(comment);
        $scope.new_comment = "";
        $scope.new_comment_id = data.comment_id;
        $ionicLoading.hide();
        // Scroll to new post
        $ionicScrollDelegate.scrollBottom(true);
      }
    });
  };
})


// CATEGORY
.controller('PostCategoryCtrl', function($scope, $rootScope, $state, $ionicLoading, $stateParams, $filter, PostService) {

  $scope.category = {};
  $scope.category.id = $stateParams.categoryId;
  $scope.category.title = $filter('translate')('HEADER_'+$stateParams.categoryTitle.toUpperCase());

  $scope.showAuthor = true;
  $scope.showContents = true;
  if($scope.category.id == 5 || $scope.category.id == 6)  {
    $scope.showAuthor = false;
    $scope.showContents = false;
  }

  $scope.posts = [];
  $scope.page = 1;
  $scope.totalPages = 1;

  $scope.doRefresh = function() {
    $ionicLoading.show({
      template: $filter('translate')('TXT_LOADING')
    });

    PostService.getPostsFromCategory($scope.category.id, 1)
    .then(function(data){
      if(data.status=='error') $window.location.reload(true);
      $scope.totalPages = data.pages;
      $scope.posts = PostService.shortenPosts(data.posts);

      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
    });
  };

  $scope.loadMoreData = function(){
    $scope.page += 1;

    PostService.getRecentPosts($scope.category.id, $scope.page)
    .then(function(data){
      //We will update this value in every request because new posts can be created
      $scope.totalPages = data.pages;
      var new_posts = PostService.shortenPosts(data.posts);
      $scope.posts = $scope.posts.concat(new_posts);

      $scope.$broadcast('scroll.infiniteScrollComplete');
    });
  };

  $scope.moreDataCanBeLoaded = function(){
    return $scope.totalPages > $scope.page;
  };

  $scope.sharePost = function(link){
    PostService.sharePost(link);
  };

  $scope.bookmarkPost = function(post){
    $ionicLoading.show({ template: 'Post Saved!', noBackdrop: true, duration: 1000 });
    PostService.bookmarkPost(post);
  };

  $scope.doRefresh();
})

;
